#include "rate_handler.hxx"

bool compare_cost_per_minute(rate* lh, rate* rh)
{
    return lh->get_cost_per_minute() < rh->get_cost_per_minute();
}

rate_handler::rate_handler(const char* file)
{
    std::ifstream ifs;
    ifs.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        ifs.open(file, std::fstream::in);
    } catch (std::ifstream::failure e) {
        std::cerr << "Exception opening file: " << std::strerror(errno) << "\n";
    }

    Json::CharReaderBuilder builder;
    Json::Value root;
    JSONCPP_STRING errs;
    builder["collectComments"] = true;

    if (!parseFromStream(builder, ifs, &root, &errs)) {
        std::cerr << errs << std::endl;
    }

    ifs.close();

    for (auto x : root) {
        if (x.isObject()) {
            rates.emplace_back(new rate(
                x["name"].asString(),
                x["time"].asInt(),
                x["cost"].asDouble(),
                x["mileage"].asInt(),
                x["overmileage"].asDouble(),
                x["waiting"].asDouble()));
        }
    }
}

rate_handler::~rate_handler()
{
    for (rate* x : rates) {
        delete x;
    }
}

void rate_handler::print_all_rates()
{
    std::for_each(rates.begin(), rates.end(), [](rate* x) { x->print(); });
}

rate rate_handler::get_min_cost_per_min_rate()
{
    auto it = std::min_element(rates.begin(), rates.end(), compare_cost_per_minute);

    return rate((*it)->get_name(),
        (*it)->get_time(),
        (*it)->get_cost(),
        (*it)->get_mileage(),
        (*it)->get_cost_overmileage(),
        (*it)->get_cost_waiting());
}
