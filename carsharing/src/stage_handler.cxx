#include "stage_handler.hxx"

//std::sort(stages.begin(), stages.end(), compare_time); //usage sample
bool compare_time(stage* lh, stage* rh)
{
    return lh->get_time() < rh->get_time();
}

stage_handler::stage_handler(const char* file)
{
    std::ifstream ifs;
    ifs.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        ifs.open(file, std::fstream::in);
    } catch (std::ifstream::failure e) {
        std::cerr << "Exception opening file: " << std::strerror(errno) << "\n";
    }

    Json::CharReaderBuilder builder;
    Json::Value root;
    JSONCPP_STRING errs;
    builder["collectComments"] = true;

    if (!parseFromStream(builder, ifs, &root, &errs)) {
        std::cerr << errs << std::endl;
    }

    ifs.close();

    for (auto x : root) {
        if (x.isObject()) {
            stages.emplace_back(new stage(x["index"].asInt(), x["time"].asInt(), x["distance"].asInt(), x["motion"].asBool()));
        }
    }
}

stage_handler::~stage_handler()
{
    for (auto stage : stages)
        delete stage;
}

void stage_handler::print_all_stages()
{
    std::for_each(stages.begin(), stages.end(), [](stage* x) { x->print(); });
}

stage stage_handler::get_min_time_stage()
{
    auto it = std::min_element(stages.begin(), stages.end(), compare_time);

    return stage((*it)->get_index(), (*it)->get_time(), (*it)->get_distance(), (*it)->get_motion());
}
