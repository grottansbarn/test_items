#include "rate.hxx"

void rate::print()
{
    std::cout << "name = \t\t" << name << std::endl
              << "time = \t\t" << time << std::endl
              << "cost = \t\t" << cost << std::endl
              << "mileage = \t\t" << mileage << std::endl
              << "overmileage = \t" << cost_overmileage << std::endl
              << "waiting = \t\t" << cost_waiting << std::endl
              << "---------------------------------------------------" << std::endl;
}

const std::string& rate::get_name()
{
    return name;
}

const int& rate::get_time()
{
    return time;
}

const double& rate::get_cost()
{
    return cost;
}

const int& rate::get_mileage()
{
    return mileage;
}

const double& rate::get_cost_overmileage()
{
    return cost_overmileage;
}

const double& rate::get_cost_waiting()
{
    return cost_waiting;
}

const double& rate::get_cost_per_km()
{
    return cost_per_km;
}

const double& rate::get_cost_per_minute()
{
    return cost_per_minute;
}
