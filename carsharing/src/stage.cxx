#include "stage.hxx"

//stage::stage(stage&& src)
//{
//    name = std::move(src.name);
//    motion = std::move(src.motion);
//    time = std::move(src.time);
//    distance = std::move(src.distance);
//}

void stage::print()
{
    std::cout << "minutes = \t\t" << index << std::endl
              << "motion = \t\t" << std::boolalpha << motion << std::endl
              << "time = \t\t" << time << std::endl
              << "distance = \t" << distance << std::endl
              << "---------------------------------------------------" << std::endl;
}

const int& stage::get_index()
{
    return index;
}

const int& stage::get_time()
{
    return time;
}

const int& stage::get_distance()
{
    return distance;
}

const bool& stage::get_motion()
{
    return motion;
}
