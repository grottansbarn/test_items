#pragma once

#include <iostream>
#include <string>

class rate {
public:
    rate(const std::string& name_,
        const int& time_,
        const double& cost_,
        const int& mileage_,
        const double& cost_overmileage_,
        const double& cost_waiting_)
        : name{ name_ }
        , time{ time_ }
        , cost{ cost_ }
        , mileage{ mileage_ }
        , cost_overmileage{ cost_overmileage_ }
        , cost_waiting{ cost_waiting_ }

    {
        cost_per_km = cost / mileage;
        cost_per_minute = cost / time;
    }

    rate() = default;
    rate(const rate&) = delete;
    const rate& operator=(const rate&) = delete;
    rate(rate&&) = default;
    rate& operator=(rate&&) = default;

    ~rate() = default;

    void print();

    const std::string& get_name();

    const int& get_time();
    const double& get_cost();
    const int& get_mileage();
    const double& get_cost_overmileage();
    const double& get_cost_waiting();
    const double& get_waiting_cost();

    const double& get_cost_per_km();
    const double& get_cost_per_minute();

private:
    std::string name;
    int time;
    double cost;
    int mileage;
    double cost_overmileage;
    double cost_waiting;
    double cost_per_km;
    double cost_per_minute;
};
