#pragma once

#include <iostream>
#include <string>

class stage {
public:
    stage(const int& index_, const int& time_, const int& distance_, const bool& motion_)
        : index{ index_ }
        , time{ time_ }
        , distance{ distance_ }
        , motion{ motion_ }
    {
    }

    stage() = default;
    ~stage() = default;
    stage(const stage&) = delete;
    const stage& operator=(const stage&) = delete;
    stage(stage&&) = default;
    stage& operator=(stage&&) = default;

    void print();

    const int& get_index();
    const int& get_time();
    const int& get_distance();
    const bool& get_motion();

private:
    int index;
    int time;
    int distance;
    bool motion;
};
