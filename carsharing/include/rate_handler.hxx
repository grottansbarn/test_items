#pragma once

#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "json/json.h"

#include "rate.hxx"

class rate_handler {
public:
    rate_handler(const char* file);
    ~rate_handler();

    rate_handler() = delete;
    rate_handler(const rate_handler&) = delete;
    const rate_handler& operator=(const rate_handler&) = delete;
    rate_handler(rate_handler&&) = delete;
    rate_handler& operator=(rate_handler&&) = delete;

    void print_all_rates();

    rate get_min_cost_per_min_rate();

private:
    std::vector<rate*> rates;
};
