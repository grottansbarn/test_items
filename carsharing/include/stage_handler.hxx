#pragma once

#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "json/json.h"

#include "stage.hxx"

class stage_handler {
public:
    stage_handler(const char* file);
    ~stage_handler();

    stage_handler() = delete;
    stage_handler(const stage_handler&) = delete;
    const stage_handler& operator=(const stage_handler&) = delete;
    stage_handler(stage_handler&&) = delete;
    stage_handler& operator=(stage_handler&&) = delete;

    void print_all_stages();

    stage get_min_time_stage();

private:
    std::vector<stage*> stages;
};
