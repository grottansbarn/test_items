// https://github.com/open-source-parsers/jsoncpp

#include <memory>

#include "rate_handler.hxx"
#include "stage_handler.hxx"

int main([[maybe_unused]] int argc, char* argv[])
{

    std::unique_ptr s_handler = std::make_unique<stage_handler>(argv[1]);
    //s_handler->print_all_stages();
    std::unique_ptr tmp_stage = std::make_unique<stage>(s_handler->get_min_time_stage());
    //tmp_stage->print();

    std::unique_ptr r_handler = std::make_unique<rate_handler>(argv[2]);
    // r_handler->print_all_rates();
    std::unique_ptr tmp_rate = std::make_unique<rate>(r_handler->get_min_cost_per_min_rate());
    tmp_rate->print();

    return EXIT_SUCCESS;
}
