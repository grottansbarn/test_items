#pragma once

#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>

#define IN
#define OUT

namespace comments_remover_space {

class comments_remover final {
public:
    comments_remover();

    comments_remover(const comments_remover&) = delete;
    comments_remover(comments_remover&&) = delete;
    comments_remover& operator=(const comments_remover&) = delete;
    comments_remover& operator=(comments_remover&&) = delete;

    ~comments_remover();

    int open(IN const char* file);

    int remove_comments();

    int clear();

    enum class state {
        text,
        close_star,
        comment,
        comment_till_end_line,
        //quotes
    };

private:
    const char* m_psz_file;
    state m_state;
    char m_char;
    char m_char_next;
    std::fstream m_fs_in;
    std::ofstream m_fs_out;
    std::stringstream m_ss_stream;
    std::unique_ptr<char[]> m_buffer;
};

} /// end of namespace comment_remover_space
