#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <algorithm>

#include "comments_remover.hxx"

namespace comments_remover_space {

comments_remover::comments_remover()
{
    m_psz_file = nullptr;
    m_state = state::text;
    m_char = ' ';
}

comments_remover::~comments_remover()
{
    if (m_fs_in)
        m_fs_in.close();
    if (m_fs_out) {
        m_fs_out.flush();
        m_fs_out.close();
    }
    if (m_ss_stream)
        m_ss_stream.clear();
}

int comments_remover::clear()
{
    m_psz_file = nullptr;

    if (m_fs_in)
        m_fs_in.close();
    if (m_fs_out) {
        m_fs_out.flush();
        m_fs_out.close();
    }
    if (m_ss_stream)
        m_ss_stream.clear();

    return 0;
}

int comments_remover::open(IN const char* file)
{
    if (m_psz_file != nullptr) {
        std::cerr << __FUNCTION__ << "() error: the object already associated with file. use clear()\n"
                  << std::endl;
        return 1;
    } else {
        m_psz_file = file;
    }

    m_fs_in.open(m_psz_file);
    if (!m_fs_in.is_open()) {
        std::cerr << __FUNCTION__ << "() error: can't open input file.\n"
                  << std::endl;
        return 1;
    }

    //creating name and open new file
    std::string prefix{ "cleaned_" };
    std::string output_file_name{ prefix + std::string(m_psz_file) };

    m_fs_out.open(output_file_name, std::ios_base::trunc);
    if (!m_fs_out.is_open()) {
        std::cerr << __FUNCTION__ << "() error: can't create output file\n"
                  << std::endl;
        return 2;
    }

    return 0;
}

int comments_remover::remove_comments()
{
    if (m_psz_file == nullptr) {
        std::cerr << __FUNCTION__ << "() error: the object wasn't associated with file yet. use open()\n"
                  << std::endl;
        return 1;
    }

    do {
        switch (m_state) {
        case state::text:
            if (m_char == '/') {
                //check next symbol
                m_char_next = m_fs_in.get();

                if (m_char_next == '/') {
                    m_state = state::comment_till_end_line;
                } else if (m_char_next == '*') {
                    m_state = state::comment;
                } else {
                    m_fs_out << m_char << m_char_next;
                }
            } else {
                m_fs_out << m_char;
            }
            break;
        case state::close_star:
            if (m_char == '/') {
                m_state = state::text;
            } else if (m_char == '*') {
                m_state = state::close_star;
            } else {
                m_state = state::comment;
            }
            break;
        case state::comment:
            if (m_char == '*') {
                m_state = state::close_star;
            } else if (m_char == '\n') {
                m_fs_out << m_char;
            }
            break;
        case state::comment_till_end_line:
            if (m_char == '\n') {
                m_state = state::text;
                m_fs_out << m_char;
            }
            break;
        }

        m_char = m_fs_in.get();

    } while (m_fs_in.good());

    m_fs_out.flush();

    return 0;
}

} ///end of namespace comments_remover_space
