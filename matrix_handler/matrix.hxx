#pragma once

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

template <typename T>
class matrix final {
public:
    matrix(const size_t& h, const size_t& w)
    {
        height = h;
        width = w;

        field.resize(height);
        for (size_t i = 0; i < height; i++) {
            field[i].resize(width);
        }

        for (size_t i = 0; i < height; i++) {
            for (size_t j = 0; j < width; j++) {
                field[i][j] = new T(i + j);
            }
        }
    }

    matrix(const size_t& h, const size_t& w, const T* data)
    {
        height = h;
        width = w;

        field.resize(height);
        for (size_t i = 0; i < height; i++) {
            field[i].resize(width);
        }

        for (size_t i = 0; i < height; i++) {
            for (size_t j = 0; j < width; j++) {
                field[i][j] = new T(data[i * width + j]);
            }
        }
    }

    ~matrix()
    {
        std::for_each(field.begin(), field.end(), [](std::vector<T*> vec) { std::for_each(vec.begin(), vec.end(), [](T* num) { delete num; }); });

        field.clear();
        field.shrink_to_fit();
    }

    matrix() = delete;
    matrix(const matrix&) = delete;
    const matrix& operator=(const matrix&) = delete;
    matrix& operator=(matrix&&) = delete;

    matrix(matrix&& temp) noexcept : height{ temp.height }, width{ temp.width }, field{ std::move(temp.field) }
    {
    }

    //    const matrix operator+(const T& data) const {
    //        std::unique_ptr<matrix<T> > temp = std::make_unique<matrix<T> >(this->get_height(), this->get_width());
    //        std::for_each(field.begin(), field.end(), [&data](std::vector<T*> vec){
    //            std::for_each(vec.begin(), vec.end(), [&data](T* num) { *num = *num + data; });
    //        }
    //        );
    //        return temp;
    //    }

    bool print()
    {
        std::cout << "====================================" << std::endl;
        std::for_each(field.begin(), field.end(), [](std::vector<T*> vec) {
            std::for_each(vec.begin(), vec.end(), [](T* num) { std::cout << *num << '\t'; });
            std::cout << '\n'
                      << std::endl;
        });

        return EXIT_SUCCESS;
    }

    const size_t& get_width() { return width; }

    const size_t& get_height() { return height; }

    const T& get_element(const size_t& i, const size_t& j) { return *field[i][j]; }

    void set_element(const size_t& i, const size_t& j, const T& data) { *field[i][j] = data; }

    const T* get_field_element_address(const size_t& i, const size_t& j) { return field[i][j]; }

private:
    size_t height;
    size_t width;
    std::vector<std::vector<T*> > field;
};
