/*
https://habr.com/ru/post/359272/
*/

#include "matrix.hxx"
#include "matrix_handler.hxx"

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    std::unique_ptr<matrix_handler<int> > handler = std::make_unique<matrix_handler<int> >();

    int arr1[6] = { 1, 2, 2, 3, 1, 1 };
    int arr2[6] = { 4, 2, 3, 1, 1, 5 };

    std::unique_ptr<matrix<int> > m1 = handler->create_matrix(2, 3, arr1);
    m1->print();

    std::unique_ptr<matrix<int> > m2 = handler->create_matrix(3, 2, arr2);
    m2->print();

    //    std::unique_ptr<matrix<int> > m3 = handler->sum_matrices(m1, m2);
    //    m3->print();

    std::unique_ptr<matrix<int> > m4 = handler->mul_matrices(m1, m2);
    m4->print();

    return EXIT_SUCCESS;
}
