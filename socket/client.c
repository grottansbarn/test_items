#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {
    int sockfd = 0;
    ssize_t n = 0;
    char recvBuff[65];
    struct sockaddr_in serv_addr;

    if(argc != 2) {
        printf("\n Usage: %s <IP of the server (127.0.0.1 by default)> \n", argv[0]);
        return 1;
    }

    // fill by '0' memory block by 'recvBuff' address with 'recvBuff' size
    memset(recvBuff, '0', sizeof(recvBuff));

    // create IPv4 socket with TCP protocol, and protocol type by default
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("\n Error : Could not create socket \n");
        return 2;
    }

    // fill by '0' buffer with 'serv_addr' size by 'serv_addr' address
    memset(&serv_addr, '0', sizeof(serv_addr));

    // set some parameters of server address structure: IPv4, port
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(5000);

    // modify IPv4 address string to full structure of 'af' address
    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr) <= 0){
        printf("\n inet_pton error occured \n");
        return 3;
    }

    // try to connect to server (host) by IP and port
    if(connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
       printf("\n Error : Connect Failed \n");
       return 4;
    }

    // read data from socket 'sockfd'
    while ((n = read(sockfd, recvBuff, sizeof(recvBuff) - 1)) > 0){
        recvBuff[n] = '\0';
        // transmit data from 'recvBuff' to stdout
        if(fputs(recvBuff, stdout) == EOF){
            printf("\n Error : Fputs error\n");
        }
    }

    if(n < 0){
        printf("\n Read error \n");
    }

    return 0;
}
