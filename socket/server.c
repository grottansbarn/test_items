// https://rtfm.co.ua/c-sokety-i-primer-modeli-client-server/
// http://samag.ru/archive/article/33

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

int main(int argc, char *argv[]) {
    int listenfd = 0;
    int connfd = 0;
    ssize_t result = 0;
    struct sockaddr_in serv_addr;
    char sendBuff[64];
    time_t ticks;

    // domen type: for IPv4 - AF_INT
    // protocol type: for TCP — SOCK_STREAM, for UDP — SOCK_DGRAM
    // protocol type by default - 0
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (listenfd == -1) return 1;

    // fill by '0' memory block by 'serv_addr' address by 'serv_addr' size
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(sendBuff, '0', sizeof(sendBuff));

    // fill fileds of serv_addr object to use in next instruction
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(5000);

    // create socket using parameters from serv_addr object (protocol, IP, port)
    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    // set maximum number of connections to socket
    listen(listenfd, 5);

    // how to check created socket on port 5000
    // netstat -a -n | grep 5000

    // wait for a connection
    while(1){
        // check the connection
        // create new socket 'connfd' immediatelly
        connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
        if (connfd == -1){
            return 2;
        }

        ticks = time(NULL);

        // write data to the sendBuff
        snprintf(sendBuff, sizeof(sendBuff), "%.24s\r\n", ctime(&ticks));

        // write sendBuff to socket
        result = write(connfd, sendBuff, strlen(sendBuff));
        if(result < (long)strlen(sendBuff))
            return 3;

        close(connfd);
        sleep(1);
    }
}
