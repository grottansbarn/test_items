/*
Напишите консольную программу, которая удаляет комментарии из программы на C++.
Пусть она читает из текстового файла, удаляет комментарии обоих видов
и записывает результат в новый текстовый файл.
Не заботьтесь о внешнем виде вывода.
Считайте что комментарии не будут содержаться в самих комментариях,
строках и символьных константах.

Предполагаем, что исходный файл имеет кодировку ASCII.
Алгоритм решения должен быть на C++ языке. Для решения задачи просьба не
использовать регулярные выражения и препроцессор. Можно использовать
 любую среду разработки. Исходные коды решения присылайте в виде zip-архива.
Исполняемый бинарный файл не нужен.

Задание будет проверяться на нашем тестовом примере в ОС Windows 10.
Предполагается, что оно должно работать правильно и без аварийных завершений.
*/

/* леонид обзор
Для джуна - норм! Но лично я хотел бы что бы человек написал по другому:
1 - идем по потоку символов и детектируем начало коментария
2 - если было начало то каждый следующий символ игнорируем, пока не встретим конец коментария
3 - если не было начало коментария то символ сразу же отправляем в поток для вывода, 
таким образом для любого размера файла входного у меня всегда константное количество памяти оперативной будет использоваться
4 - ну и я бы не испльзовал исключения т.к. в данном случае это не исключения а обычное дело - файла нету и просто возвращал бы стандартные текстовые сообщения на такие дела

Про исключения - например у меня на работе мы их используем так,
1 - скачали обновления
2 - проверили что црц32 везде верный и размеры файлов
3 - заходим в бой и загружаем только что скаченный новый танк
4 - А тут "..." ОС говорит что нету такого-то файла! - Я думаю как так-то! только что скачал ведь - и кидаю исключение
*/

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace grottans {

bool load_txt_filter_comments_save_new_file(const std::string_view& input_file)
{
    bool result = true;

    std::stringstream out;
    std::string line;

    std::ifstream in_stream(input_file.data());
    if (!in_stream) {
        std::cerr << std::string("can't open file ") + input_file.data();
        return false;
    }

    bool comments_searching_loop = true;
    bool inside_comments = false;
    size_t comment_left_hand_pos = std::string::npos;
    size_t comment_right_hand_pos = std::string::npos;
    size_t comment_classic_pos = std::string::npos;

    while (std::getline(in_stream, line)) {

        comments_searching_loop = true;

        comment_left_hand_pos = std::string::npos;
        comment_right_hand_pos = std::string::npos;
        comment_classic_pos = std::string::npos;

        size_t iteration = 0;

        while (comments_searching_loop) {

            iteration++;

            comment_left_hand_pos = line.find("/*");

            comment_right_hand_pos = line.find("*/");

            ///blocking error if */ before /*
            if ((comment_right_hand_pos < comment_left_hand_pos) && comment_left_hand_pos != std::string::npos) {
                comment_left_hand_pos = std::string::npos;
                inside_comments = false;
            }

            ///line erase
            ///if in line both side of comment
            if (comment_left_hand_pos != std::string::npos && comment_right_hand_pos != std::string::npos) {
                line = line.substr(0, comment_left_hand_pos) + line.substr(comment_right_hand_pos + 2, line.length() - 1);
                continue;
            }
            ///if only left hand side
            if (comment_left_hand_pos != std::string::npos && comment_right_hand_pos == std::string::npos) {
                line = line.substr(0, comment_left_hand_pos);
                inside_comments = true;
                continue;
            }
            ///if only right hand side
            if (comment_left_hand_pos == std::string::npos && comment_right_hand_pos != std::string::npos) {
                line = line.substr(comment_right_hand_pos + 2, line.length() - 1);
                inside_comments = false;
                continue;
            }
            ///if no one hand side finded
            if (comment_left_hand_pos == std::string::npos && comment_right_hand_pos == std::string::npos) {
                comments_searching_loop = false;
                continue;
            }

        } ///end of searching /**/ comments (while)

        ///delete the lines inside the comment
        if (inside_comments && iteration == 1) {
            line = "";
        }

        comment_classic_pos = line.find("//");
        if (comment_classic_pos != std::string::npos) { //if find "//"
            line = line.substr(0, comment_classic_pos);
        }

        ///push cleaned line
        out << line << '\n';

    } ///end of file cleaning (while)

    in_stream.close();

    ///creating and saving new file
    std::string prefix = "comments_removed_";
    std::string output_file_name = prefix + input_file.data();

    std::ofstream ofstr_output_file;
    ofstr_output_file.open(output_file_name);
    if (ofstr_output_file.is_open()) {
        ofstr_output_file << out.rdbuf() << std::endl;
    } else {
        std::cerr << (std::string("can't create output file: ") + output_file_name);
        result = false;
    }

    return result;
}

} // end of namespace grottans

///////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    if (argc == 1) {
        std::cerr << "error: input file path not specified\n";
    }

    std::string_view path = argv[1];

    return grottans::load_txt_filter_comments_save_new_file(path) ? EXIT_SUCCESS : EXIT_FAILURE;
}
