#pragma once

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "../app/pcap_handler.hxx"

using namespace testing;

TEST(pcap_tests, clear_00)
{
    //    std::unique_ptr<pcap_test_item::pcap_handler> handl = std::make_unique<pcap_test_item::pcap_handler>();
    //    handl->open("./test.pcap");
    //    handl->parse();
    //    handl->clear();
    //    EXPECT_EQ(handl->m_ul_packets, 0);
}

TEST(pcap_tests, open_00)
{
    std::unique_ptr<pcap_test_item::pcap_handler> handl = std::make_unique<pcap_test_item::pcap_handler>();
    EXPECT_EQ(handl->open("./test.pcap"), 0);
}

TEST(pcap_tests, open_01)
{
    std::unique_ptr<pcap_test_item::pcap_handler> handl = std::make_unique<pcap_test_item::pcap_handler>();
    handl->open("./test.pcap");
    EXPECT_EQ(handl->open("./test.pcap"), 1);
}

TEST(pcap_tests, parse_00)
{
    std::unique_ptr<pcap_test_item::pcap_handler> handl = std::make_unique<pcap_test_item::pcap_handler>();
    EXPECT_EQ(handl->parse(), 1);
}

TEST(pcap_tests, parse_01)
{
    std::unique_ptr<pcap_test_item::pcap_handler> handl = std::make_unique<pcap_test_item::pcap_handler>();
    handl->open("./test.pcap");
    EXPECT_EQ(handl->parse(), 0);
}

TEST(pcap_tests, output_statistics_00)
{
    std::unique_ptr<pcap_test_item::pcap_handler> handl = std::make_unique<pcap_test_item::pcap_handler>();
    EXPECT_EQ(handl->output_statistics(), 1);
}

TEST(pcap_tests, output_statistics_01)
{
    std::unique_ptr<pcap_test_item::pcap_handler> handl = std::make_unique<pcap_test_item::pcap_handler>();
    handl->open("./test.pcap");
    handl->parse();
    EXPECT_EQ(handl->output_statistics(), 0);
}
