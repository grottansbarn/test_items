#include <algorithm>

#include "./pcap_handler.hxx"

namespace pcap_test_item {

char* pcap_handler::ether_ntoa_r(IN const ether_addr* addr, OUT char* buf)
{
    sprintf(buf, "%x:%x:%x:%x:%x:%x",
        addr->ether_addr_octet[0], addr->ether_addr_octet[1],
        addr->ether_addr_octet[2], addr->ether_addr_octet[3],
        addr->ether_addr_octet[4], addr->ether_addr_octet[5]);
    return buf;
}

char* pcap_handler::ether_ntoa(IN OUT const ether_addr* addr)
{
    static char asc[18];
    return ether_ntoa_r(addr, asc);
}

int pcap_handler::clear()
{
    m_ul_packets = 0;
    m_psz_file = "";
    m_map_peers.clear();

    return 0;
}

int pcap_handler::open(const char* const input_file)
{
    if (m_psz_file != nullptr) {
        std::cerr << __FUNCTION__ << "() error: the object already associated with file. use clear()\n"
                  << std::endl;
        return 1;
    }

    m_psz_file = input_file;

    char errbuff[PCAP_ERRBUF_SIZE];

    m_pcap_file_descriptor = pcap_open_offline(m_psz_file, errbuff);

    if (m_pcap_file_descriptor == nullptr) {
        std::cerr << __FUNCTION__ << "() error: " << errbuff << std::endl;
        return 2;
    }

    return 0;
}

int pcap_handler::parse()
{
    if (m_psz_file == nullptr || m_pcap_file_descriptor == nullptr) {
        std::cerr << __FUNCTION__ << "() error: the object wasn't associated with file yet. use open()\n"
                  << std::endl;
        return 1;
    }

    //The header that pcap gives us
    struct pcap_pkthdr* header;

    //The actual packet
    const u_char* packet;

    //Ethernet header
    const sniff_ethernet* ethernet;

    //Cyclic variables initialization
    auto it{ m_map_peers.end() };
    char* temp_src_mac{ nullptr };

    while (pcap_next_ex(m_pcap_file_descriptor, &header, &packet) >= 0) {

        ethernet = (sniff_ethernet*)(packet);

        m_ul_packets++;

        temp_src_mac = ether_ntoa((ether_addr*)ethernet->ether_shost);

        //inserting unique peer into map
        //or adding size of the packet to existing peer
        it = m_map_peers.find(temp_src_mac);
        if (it != m_map_peers.end()) {
            it->second.first += 1;
            it->second.second += header->len;
        } else {
            m_map_peers[temp_src_mac] = std::make_pair(1, header->len);
        }
    }

    return 0;
}

int pcap_handler::cap_output()
{
    if (m_psz_file == nullptr) {
        std::cerr << __FUNCTION__ << "() error: the object wasn't associated with file yet. use open()\n"
                  << std::endl;
        return 1;
    }

    std::cout << "\nSummary\n"
              << "File: " << m_psz_file << '\n'
              << "Processed packets: " << m_ul_packets
              << std::endl;

    if (std::cout.bad()) {
        std::cerr << __FUNCTION__ << "() warning: problem with std::cout\n"
                  << std::endl;
    }
    return 0;
}

int pcap_handler::output_statistics()
{
    if (m_map_peers.empty()) {
        std::cerr << __FUNCTION__ << "() error: pcap file wasn't parsed yet. use parse()\n"
                  << std::endl;
        return 1;
    }

    std::cout << "Peers:" << std::endl;
    if (std::cout.bad()) {
        std::cerr << __FUNCTION__ << "() warning: problem with std::cout\n"
                  << std::endl;
    }

    auto it{ m_map_peers.begin() };

    for (size_t i{ 1 }; it != m_map_peers.end(); it++, i++) {
        std::cout << i << ") mac " << it->first
                  << ", packets " << it->second.first
                  << ", bytes " << it->second.second << std::endl;

        if (std::cout.bad()) {
            std::cerr << __FUNCTION__ << "() warning: problem with std::cout\n"
                      << std::endl;
        }
    }

    return 0;
}

} ///end of namespace
