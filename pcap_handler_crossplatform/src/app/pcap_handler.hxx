#pragma once

#include <iostream>
#include <map>
#include <string>

#include "pcap/pcap.h"

#define IN
#define OUT

#define ETHER_ADDR_LEN 6

namespace pcap_test_item {

typedef std::map<std::string, std::pair<size_t, size_t> > MAP;

struct ether_addr {
    uint8_t ether_addr_octet[6];
};

struct sniff_ethernet { /* Ethernet header */
    u_char ether_dhost[ETHER_ADDR_LEN]; /* Destination address */
    u_char ether_shost[ETHER_ADDR_LEN]; /* Source address */
    u_short ether_type; /* IP? ARP? RARP? etc */
};

class pcap_handler final {
public:
    char* ether_ntoa_r(IN const ether_addr* addr, OUT char* buf);

    char* ether_ntoa(IN OUT const ether_addr* addr);

    pcap_handler()
        : m_psz_file{ nullptr }
        , m_pcap_file_descriptor{ nullptr }
        , m_ul_packets{ 0 }
    {
    }

    pcap_handler(IN const char* const input_file)
        : m_psz_file{ input_file }
        , m_pcap_file_descriptor{ nullptr }
        , m_ul_packets{ 0 }
    {
    }

    pcap_handler(const pcap_handler&) = delete;
    pcap_handler(pcap_handler&&) = delete;
    pcap_handler& operator=(const pcap_handler&) = delete;
    pcap_handler& operator=(pcap_handler&&) = delete;

    ~pcap_handler() {}

    /*! @brief Clear internal variables before parsing file.*/
    int clear();

    /*! @brief Open pcap file and set internal descriptor.*/
    int open(const char* const input_file);

    /*! @brief Parse pcap file.*/
    int parse();

    /*! @brief Output the common information about processed pcap file.*/
    int cap_output();

    /*! @brief Output summary information about peers.*/
    int output_statistics();

private:
    const char* m_psz_file;
    pcap_t* m_pcap_file_descriptor;
    size_t m_ul_packets;
    MAP m_map_peers;
};

} /// end of namespace pcap_test_item
