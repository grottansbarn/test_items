Сonsole utility for analyzing and displaying in the console information about peers (MAC address) from * .pcap files (created by Wireshark).
The file path must be specified as an argument when running the utility from the command line.
Sample for Linux:
./pcap_handler_crossplatform test.pcap

Building:
$ mkdir build
$ cd build
$ cmake ..
$ make
