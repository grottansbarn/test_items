/*
https://www.hackerrank.com/challenges/attribute-parser/problem

Sample Input
4 3
<tag1 value = "HelloWorld">
<tag2 name = "Name1">
</tag2>
</tag1>
tag1.tag2~name
tag1~name
tag1~value

Sample Output
Name1
Not Found!
HelloWorld

*/

#include "attribute_parser.hxx"
#include "attribute_parser_handler.hxx"


int main ([[maybe_unused]] int argc, [[maybe_unused]] char *argv[])
{
    std::unique_ptr<attribute_parser_handler<int> > handler = std::make_unique<attribute_parser_handler<int> >();

    int arr1[6] = {1, 2, 2, 3, 1, 1};
    int arr2[6] = {4, 2, 3, 1, 1, 5};

    std::unique_ptr<attribute_parser<int> > m1 = handler->create_matrix(2, 3, arr1);
    m1->print();

    std::unique_ptr<attribute_parser<int> > m2 = handler->create_matrix(3, 2, arr2);
    m2->print();

//    std::unique_ptr<attribute_parser<int> > m3 = handler->sum_matrices(m1, m2);
//    m3->print();

    std::unique_ptr<attribute_parser<int> > m4 = handler->mul_matrices(m1, m2);
    m4->print();

    return EXIT_SUCCESS;
}
