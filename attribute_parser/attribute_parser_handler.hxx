#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
#include <immintrin.h>

#include "attribute_parser.hxx"

template <typename T>
class attribute_parser_handler final{
public:
    attribute_parser_handler() = default;

    ~attribute_parser_handler() = default;

    std::unique_ptr<attribute_parser<T> > create_matrix(const size_t& h, const size_t& w)
    {
        return std::make_unique<attribute_parser<T> >(h, w);
    }

    std::unique_ptr<attribute_parser<T> > create_matrix(const size_t& h, const size_t& w, const T* arr)
    {
        return std::make_unique<attribute_parser<T> >(h, w, arr);
    }

    bool check_feasibility_sum(
            const std::unique_ptr<attribute_parser<T> >& left,
            const std::unique_ptr<attribute_parser<T> >& right)
    {
        return (left->get_width() == right->get_width()) && (left->get_height() == right->get_height());
    }

    bool check_feasibility_mul(
            const std::unique_ptr<attribute_parser<T> >& left,
            const std::unique_ptr<attribute_parser<T> >& right)
    {
        return (left->get_height() == right->get_width());
    }

    std::unique_ptr<attribute_parser<T> > sum_matrices(
            const std::unique_ptr<attribute_parser<T> >& left,
            const std::unique_ptr<attribute_parser<T> >& right)
    {
        if(!check_feasibility_sum(left, right)){
            throw std::runtime_error("can't to sum matrices - size error");
        }

        auto temp = std::make_unique<attribute_parser<T> >(left->get_width(), left->get_height());

        size_t width = left->get_width();
        size_t height = left->get_height();

        for (size_t i = 0; i < width; i++) {
            for (size_t j = 0; j < height; j++) {
                temp->set_element(i, j, left->get_element(i, j) + right->get_element(i, j));
            }
        }

        return temp;
    }

    std::unique_ptr<attribute_parser<T> > mul_matrices(
            const std::unique_ptr<attribute_parser<T> >& left,
            const std::unique_ptr<attribute_parser<T> >& right)
    {
        if(!check_feasibility_mul(left, right)){
            throw std::runtime_error("can't to mul matrices - size error");
        }

        auto temp = std::make_unique<attribute_parser<T> >(left->get_height(), right->get_width());

        size_t left_h = left->get_height();
        size_t left_w = left->get_width();
        size_t right_w = right->get_width();

        ////////////////////////////////////////////

        for (size_t i = 0; i < left_h; i++)
        {
            for (size_t j = 0; j < right_w; j++)
            {
                temp->set_element(i, j, 0);
                for (size_t k = 0; k < left_w; k++){
                    T tmp1 = left->get_element(i , k) * right->get_element(k, j);
                    T tmp2 = temp->get_element(i , j);
                    temp->set_element(i, j, tmp1 + tmp2);
                }
            }
        }

        return temp;
    }

    attribute_parser_handler(const attribute_parser_handler&) = delete;

    const attribute_parser_handler& operator=(const attribute_parser_handler&) = delete;

    attribute_parser_handler(attribute_parser_handler&& temp) noexcept
    {
        std::move(temp);
    }

    attribute_parser_handler& operator=(attribute_parser_handler&&) = delete;
};
